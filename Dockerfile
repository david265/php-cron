FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
  cron \
  supervisor

# Configurate different process to execute
COPY config/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
COPY config/crontab /etc/cron.d/crontab

# Run supervisor in order to have cron + php-fpm running.
# See https://docs.docker.com/config/containers/multi-service_container/
CMD ["/usr/bin/supervisord", "-c/etc/supervisor/conf.d/supervisord.conf"]
